package edu.crabdev.training.particles.core;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class Particle {

    private int size = 10;
    private double dx = 5;
    private double dy = 5;

    Ellipse2D shape = new Ellipse2D.Double();

    public Particle() {
    }

    public Particle(double x, double y) {
        shape.setFrame(x, y, size, size);
    }

    public Particle(double x, double y, double dx, double dy) {
        this(x, y);
        this.dx = dx;
        this.dy = dy;
    }

    public Particle(double x, double y, double dx, double dy, int size) {
        this.dx = dx;
        this.dy = dy;
        this.size = size;
        shape.setFrame(x, y, size, size);
    }

    public void move(Rectangle2D bounds) {
        double x = shape.getX();
        double y = shape.getY();
        x += dx;
        y += dy;
        if (x < bounds.getMinX()) {
            x = bounds.getMinX();
            dx = -dx;
        }
        if (x + size >= bounds.getMaxX()) {
            x = bounds.getMaxX() - size;
            dx = -dx;
        }
        if (y < bounds.getMinY()) {
            y = bounds.getMinY();
            dy = -dy;
        }
        if (y + size >= bounds.getMaxY()) {
            y = bounds.getMaxY() - size;
            dy = -dy;
        }
        shape.setFrame(x, y, size, size);
    }

    public Ellipse2D getShape() {
        return shape;
    }

}
