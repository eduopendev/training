package edu.crabdev.training.particles.core;

import edu.crabdev.training.Utils;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Engine {

    private static final Engine instance = new Engine();

    private static final int DELAY = 20;
    private Canvas canvas = new Canvas();
    private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> scheduledFuture;

    private List<Particle> particles = new ArrayList<>();

    private Runnable bgTask = () -> {
        particles.forEach(particle -> particle.move(canvas.getBounds()));
        canvas.repaint();
    };

    private Engine() {
    }

    public static Engine getInstance() {
        return instance;
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public void start() {
        if (scheduledFuture == null) {
            System.out.println("engine start");
            scheduledFuture = executor.scheduleWithFixedDelay(bgTask, 0, DELAY, TimeUnit.MILLISECONDS);
        }
    }

    public void stop() {
        if (scheduledFuture != null && scheduledFuture.cancel(true)) {
            System.out.println("engine stop");
            scheduledFuture = null;
        }
    }

    public void init() {
        stop();
        particles.clear();
        Dimension size = canvas.getSize();
        System.out.println(size);
        for (int i = 0; i < 15; i++) {
            particles.add(new Particle(
                    Utils.rndAB(0, size.width),
                    Utils.rndAB(0, size.height),
                    Utils.rndAB(1, 15),
                    Utils.rndAB(1, 15),
                    Utils.rndAB(10, 20)
            ));
        }
        canvas.init(particles.stream().map(Particle::getShape).collect(Collectors.toList()));
    }
}
