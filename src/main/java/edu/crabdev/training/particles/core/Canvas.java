package edu.crabdev.training.particles.core;

import edu.crabdev.training.Utils;

import javax.swing.*;
import javax.swing.plaf.BorderUIResource;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Canvas extends JPanel {

    private List<Shape> shapes = new ArrayList<>();

    Canvas() {
        setBorder(BorderUIResource.getBlackLineBorderUIResource());
        setBackground(Color.WHITE);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        shapes.forEach(g2::fill);
    }

    void init(List<Shape> shapes) {
        this.shapes = shapes;
        repaint();
    }
}
