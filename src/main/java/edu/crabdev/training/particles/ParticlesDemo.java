package edu.crabdev.training.particles;

import edu.crabdev.training.particles.frames.MainFrame;

import javax.swing.*;
import java.awt.*;

public class ParticlesDemo {

    public static void main(String[] args) {

        EventQueue.invokeLater(() -> {
            JFrame frame  = new MainFrame();
            frame.setTitle("ParticlesDemo");
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setLocationByPlatform(true);
            frame.setVisible(true);
        });

    }

}
