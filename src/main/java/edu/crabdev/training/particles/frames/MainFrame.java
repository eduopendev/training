package edu.crabdev.training.particles.frames;

import edu.crabdev.training.Utils;
import edu.crabdev.training.particles.core.Canvas;
import edu.crabdev.training.particles.core.Engine;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {

    private final Engine engine = Engine.getInstance();
    private final Canvas canvas = engine.getCanvas();

    public MainFrame() throws HeadlessException {
        add(canvas, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(Utils.makeButton("Старт", e -> engine.start()));
        buttonPanel.add(Utils.makeButton("Стоп", e -> engine.stop()));
        buttonPanel.add(Utils.makeButton("Новый набор частиц", e -> engine.init()));

        add(buttonPanel, BorderLayout.SOUTH);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize.width / 2, screenSize.height / 2);
    }


    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        engine.init();
    }



}
