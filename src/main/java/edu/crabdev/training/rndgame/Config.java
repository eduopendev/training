package edu.crabdev.training.rndgame;

public class Config {

    private int a = 1;
    private int b = 100;
    private int limit = 1000;

    public Config() {
    }

    public Config(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public Config(int a, int b, int limit) {
        this.a = a;
        this.b = b;
        this.limit = limit;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getLimit() {
        return limit;
    }
}
