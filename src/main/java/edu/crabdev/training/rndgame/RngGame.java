package edu.crabdev.training.rndgame;

import edu.crabdev.training.Utils;
import edu.crabdev.training.rndgame.players.Player;
import java.util.Random;

public class RngGame {

    private Config config = new Config();
    private Player player;

    private int counter = 0;
    private int x;

    public RngGame() {
    }

    public RngGame(Config config) {
        this.config = config;
    }

    public Config getConfig() {
        return config;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void start() {
        if (player != null) {
            Random rnd = new Random();
            this.x = Utils.rndAB(config.getA(), config.getB());

            System.out.println("Отгадайте число от 1 до 100");
            int feedback = 0;
            while (true) {
                int input = player.getAnswer(feedback);
                counter++;
                if (input < x) {
                    feedback = -1;
                    System.out.println("мало!");
                } else if (input > x) {
                    feedback = 1;
                    System.out.println("много!");
                } else {
                    System.out.println("Победа! Число попыток: " + counter);
                    break;
                }
            }

        }
    }
}
