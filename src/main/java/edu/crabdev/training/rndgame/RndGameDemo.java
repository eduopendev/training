package edu.crabdev.training.rndgame;

import edu.crabdev.training.rndgame.RngGame;
import edu.crabdev.training.rndgame.players.ForceBot;
import edu.crabdev.training.rndgame.players.RndBot;

public class RndGameDemo {

    public static void main(String[] args) {
        RngGame rngGame = new RngGame();
        rngGame.setPlayer(new RndBot(rngGame.getConfig()));
        rngGame.start();
    }

}
