package edu.crabdev.training.rndgame.players;

import edu.crabdev.training.rndgame.Config;

public class ForceBot extends AbstractPlayer {

    int answer = this.config.getA();

    public ForceBot(Config config) {
        super(config);
    }

    public ForceBot() {

    }

    @Override
    public int getAnswer(int feedback) {
        return ++answer;
    }

}
