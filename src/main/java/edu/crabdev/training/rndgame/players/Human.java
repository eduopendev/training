package edu.crabdev.training.rndgame.players;

import edu.crabdev.training.rndgame.Config;

import java.util.Scanner;

public class Human extends AbstractPlayer {

    public Human(Config config) {
        super(config);
    }

    public Human() {

    }

    @Override
    public int getAnswer(int feedback) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("ваш ответ:");
        int input = scanner.nextInt();
        return input;
    }

}
