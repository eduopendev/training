package edu.crabdev.training.rndgame.players;

import edu.crabdev.training.rndgame.Config;
import edu.crabdev.training.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RndBot extends AbstractPlayer {

    List<Integer> answers = new ArrayList<>();

    public RndBot(Config config) {
        super(config);
    }

    public RndBot() {
    }

    @Override
    public int getAnswer(int feedback) {
        Random random = new Random();
        int answer;
        do {
            answer = Utils.rndAB(config.getA(), config.getB());
        } while(answers.contains(answer));
        answers.add(answer);
        return answer;
    }

}
