package edu.crabdev.training.rndgame.players;

import edu.crabdev.training.rndgame.Config;

public interface Player {

    int getAnswer(int feedback);

    Config getConfig();
    void setConfig(Config config);
}
