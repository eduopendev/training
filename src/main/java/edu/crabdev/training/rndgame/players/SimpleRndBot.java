package edu.crabdev.training.rndgame.players;

import edu.crabdev.training.rndgame.Config;
import edu.crabdev.training.Utils;

import java.util.Random;

public class SimpleRndBot extends AbstractPlayer {

    public SimpleRndBot(Config config) {
        super(config);
    }

    public SimpleRndBot() {

    }

    @Override
    public int getAnswer(int feedback) {
        Random random = new Random();
        return Utils.rndAB(config.getA(), config.getB());
    }

}