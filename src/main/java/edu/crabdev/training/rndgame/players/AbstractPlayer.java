package edu.crabdev.training.rndgame.players;

import edu.crabdev.training.rndgame.Config;

public abstract class AbstractPlayer implements Player {

    protected Config config = new Config();

    protected AbstractPlayer(Config config) {
        this.config = config;
    }

    public AbstractPlayer() {
    }

    @Override
    public Config getConfig() {
        return config;
    }

    @Override
    public void setConfig(Config config) {
        this.config = config;
    }
}
