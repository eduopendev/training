package edu.crabdev.training;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.Random;

public class Utils {

    private Utils() {
    }

    public static int rndAB(int a, int b) {
        Random rnd = new Random();
        return a + rnd.nextInt(b - a + 1);
    }

    public static JButton makeButton(String title, ActionListener actionListener) {
        JButton button = new JButton(title);
        button.addActionListener(actionListener);
        return button;
    }
}
