package edu.crabdev.training.paint;

import edu.crabdev.training.paint.frames.MainFrame;

import javax.swing.*;
import java.awt.*;

public class PaintDemo {

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JFrame frame  = new MainFrame("PaintDemo");
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setLocationByPlatform(true);
            frame.setVisible(true);
        });
    }

}
