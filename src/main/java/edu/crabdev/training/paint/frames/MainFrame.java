package edu.crabdev.training.paint.frames;

import edu.crabdev.training.paint.core.Canvas;

import javax.swing.*;
import javax.swing.plaf.BorderUIResource;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainFrame extends JFrame {

    private final JLabel status = new JLabel("статусная строка");
    private final Canvas canvas = new Canvas();

    public MainFrame(String title) throws HeadlessException {
        super(title);

        canvas.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                status.setText(String.format("Нажатие мышки по координатам x:%d, y:%d", e.getX(), e.getY()));
            }
        });
        canvas.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);
                status.setText(String.format("координаты мыши x:%d, y:%d", e.getX(), e.getY()));
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                status.setText(String.format("перетаскивание x:%d, y:%d", e.getX(), e.getY()));
                canvas.getGraphics().drawLine(e.getX(), e.getY(), e.getX(), e.getY());
            }
        });

        add(canvas);

        JPanel statusPanel = new JPanel();
        statusPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        statusPanel.setBorder(BorderUIResource.getLoweredBevelBorderUIResource());
        statusPanel.add(status);

        add(statusPanel, BorderLayout.SOUTH);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize.width / 2, screenSize.height / 2);
    }

}
