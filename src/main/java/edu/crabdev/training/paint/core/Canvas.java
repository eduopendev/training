package edu.crabdev.training.paint.core;

import javax.swing.*;
import javax.swing.plaf.BorderUIResource;
import java.awt.*;

public class Canvas extends JPanel {

    public Canvas() {
        setBorder(BorderUIResource.getBlackLineBorderUIResource());
        setBackground(Color.WHITE);
    }

}
