package edu.crabdev.training.cellular.frames;

import edu.crabdev.training.Utils;
import edu.crabdev.training.cellular.core.Canvas;
import edu.crabdev.training.cellular.core.Engine;
import edu.crabdev.training.cellular.core.GUIUpdater;

import javax.swing.*;
import javax.swing.plaf.BorderUIResource;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainFrame extends JFrame {

    private final Engine engine = Engine.getInstance();
    private final JLabel statusLabel = new JLabel("статусная строка");
    private final JLabel genNumLabel = new JLabel("Номер поколения");
    private final JLabel genNumValueLabel = new JLabel("0");
    private final JLabel genLiveLabel = new JLabel("Численность популяции");
    private final JLabel genLiveValueLabel = new JLabel("0");


    public MainFrame(String title) throws HeadlessException {
        super(title);

        engine.setGuiUpdater(new GUIUpdater() {
            @Override
            public void updatePopulationNumber(int value) {
                genNumValueLabel.setText(String.valueOf(value));
            }

            @Override
            public void updateCountOfAlive(int value) {
                genLiveValueLabel.setText(String.valueOf(value));
            }
        });

        Canvas canvas = engine.getCanvas();
        canvas.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                if (engine.isRunning()) {
                    statusLabel.setText("Нельзя изменять популяцию в процессе эволюции. Жмите стоп.");
                } else {
                    engine.toggleCell(e.getPoint());
                }
            }
        });
        canvas.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);
                Point cellPoint = engine.getCellPoint(e.getPoint());
                statusLabel.setText(String.format("Ячейка (%d, %d)", cellPoint.x + 1, cellPoint.y + 1));
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                if (!engine.isRunning()) {
                    engine.fillCell(e.getPoint());
                }

            }
        });

        add(canvas);

        final int CONTROL_PANEL_WIDTH = 220;
        final int STATUS_PANEL_HEIGHT = 30;
        JPanel control = new JPanel();
        control.setPreferredSize(new Dimension(CONTROL_PANEL_WIDTH, 0));
        control.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 20));
        control.add(Utils.makeButton("Старт", e -> engine.start()));
        control.add(Utils.makeButton("Стоп", e -> engine.stop()));
        control.add(genNumLabel);
        control.add(genNumValueLabel);
        control.add(genLiveLabel);
        control.add(genLiveValueLabel);
        control.add(Utils.makeButton("Сброс", e -> engine.reset()));

        add(control, BorderLayout.EAST);

        JPanel statusPanel = new JPanel();
        statusPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        statusPanel.setPreferredSize(new Dimension(0, STATUS_PANEL_HEIGHT));
        statusPanel.add(statusLabel);
        statusPanel.setBorder(BorderUIResource.getLoweredBevelBorderUIResource());
        add(statusPanel, BorderLayout.SOUTH);

        setSize(canvas.getPreferredSize().width + CONTROL_PANEL_WIDTH + 4, canvas.getPreferredSize().height + STATUS_PANEL_HEIGHT + 25);
        centeredFrame(this);
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        engine.init();

    }

    private void centeredFrame(JFrame frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (dimension.width - frame.getWidth()) / 2;
        int y = (dimension.height - frame.getHeight()) / 2;
        frame.setLocation(x, y);
    }

}
