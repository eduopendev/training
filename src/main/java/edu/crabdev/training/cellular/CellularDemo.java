package edu.crabdev.training.cellular;

import edu.crabdev.training.cellular.frames.MainFrame;

import javax.swing.*;
import java.awt.*;

public class CellularDemo {

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JFrame frame = new MainFrame("CellularDemo");
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setResizable(false);
            frame.setVisible(true);
        });
    }

}
