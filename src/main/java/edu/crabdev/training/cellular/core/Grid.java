package edu.crabdev.training.cellular.core;

class Grid {

    private Cell[][] cells;

    private int columns;
    private int rows;
    private int alive = 0;

    Grid(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        init();
    }

    private void init() {
        cells = new Cell[rows][columns];
        alive = 0;
    }

    void clear() {
        init();
    }

    int getColumns() {
        return columns;
    }

    int getRows() {
        return rows;
    }

    boolean isAliveCell(int row, int column) {
        Cell cell = getCell(row, column);
        return cell != null && cell.isLive();
    }

    boolean isEmpty(int row, int column) {
        Cell cell = getCell(row, column);
        return cell == null;
    }

    Cell getCell(int row, int column) {
        Cell cell = null;
        //TODO добавить периодические граничные условия
        try {

            row = row < 0 ? rows - 1 : (row >= rows ? 0 : row);
            column = column < 0 ? columns - 1 : (column >= columns ? 0 : column);

            cell = cells[row][column];
        } catch (IndexOutOfBoundsException e) {
            //e.printStackTrace();
        }
        return cell;
    }

    void fillCell(int row, int column) {
        if(!isAliveCell(row, column)) {
            cells[row][column] = new Cell(true);
            alive++;
        }
    }

    void killCell(int row, int column) {
        cells[row][column] = null;
        alive--;
    }

    int getAlive() {
        return alive;
    }

}
