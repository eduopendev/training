package edu.crabdev.training.cellular.core;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Engine {

    private static final Engine instance = new Engine();

    private static final int ROWS = 40;
    private static final int COLUMNS = 60;
    private static final int CELL_SIZE = 15;

    private Grid grid = new Grid(ROWS, COLUMNS);

    private final Canvas canvas = new Canvas(grid, CELL_SIZE);
    private static final int DELAY = 500;
    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> scheduledFuture;
    private boolean isRunning = false;

    private int genNum = 0;

    private GUIUpdater guiUpdater;

    private Engine() {
    }

    private Runnable bgTask = () -> {
        evolve();
        genNum++;
        updateGUI();
    };

    public static Engine getInstance() {
        return instance;
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public void setGuiUpdater(GUIUpdater guiUpdater) {
        this.guiUpdater = guiUpdater;
    }

    public void start() {
        if (scheduledFuture == null) {
            System.out.println("engine start");
            scheduledFuture = executor.scheduleWithFixedDelay(bgTask, 0, DELAY, TimeUnit.MILLISECONDS);
            isRunning = true;
        }
    }

    public void stop() {
        if (scheduledFuture != null && scheduledFuture.cancel(true)) {
            System.out.println("engine stop");
            scheduledFuture = null;
            isRunning = false;
        }
    }

    public void init() {
        stop();
        canvas.init();
    }

    public void reset() {
        init();
        genNum = 0;
        updateGUI();
    }

    public Point getCellPoint(Point point) {
        return new Point(point.y / CELL_SIZE, point.x / CELL_SIZE);
    }

    public boolean isRunning() {
        return isRunning;
    }

    private void updateGUI() {
        canvas.repaint();
        if (guiUpdater != null) {
            SwingUtilities.invokeLater(() -> {
                guiUpdater.updatePopulationNumber(genNum);
                guiUpdater.updateCountOfAlive(grid.getAlive());
            });
        }
    }

    public void toggleCell(Point point) {
        Point cp = getCellPoint(point);
        if (grid.isAliveCell(cp.x, cp.y)) {
            grid.killCell(cp.x, cp.y);
        } else {
            grid.fillCell(cp.x, cp.y);
        }
        updateGUI();
    }

    public void fillCell(Point point) {
        Point cp = getCellPoint(point);
        grid.fillCell(cp.x, cp.y);
        updateGUI();
    }

    private void evolve() {


        class CellPoint {
            private int row;
            private int column;
            private boolean isAlive;

            private CellPoint(int row, int column, boolean isAlive) {
                this.row = row;
                this.column = column;
                this.isAlive = isAlive;
            }
        }

        List<CellPoint> affected = new ArrayList<>();

        for (int column = 0; column < grid.getColumns(); column++) {
            for (int row = 0; row < grid.getRows(); row++) {

                int countOfNeighbors = 0;

                if (grid.isAliveCell(row - 1, column)) {
                    countOfNeighbors++;
                }
                if (grid.isAliveCell(row - 1, column + 1)) {
                    countOfNeighbors++;
                }
                if (grid.isAliveCell(row, column + 1)) {
                    countOfNeighbors++;
                }
                if (grid.isAliveCell(row + 1, column + 1)) {
                    countOfNeighbors++;
                }
                if (grid.isAliveCell(row + 1, column)) {
                    countOfNeighbors++;
                }
                if (grid.isAliveCell(row + 1, column - 1)) {
                    countOfNeighbors++;
                }
                if (grid.isAliveCell(row, column - 1)) {
                    countOfNeighbors++;
                }
                if (grid.isAliveCell(row - 1, column - 1)) {
                    countOfNeighbors++;
                }

                if (countOfNeighbors == 3 && grid.isEmpty(row, column)) {
                    affected.add(new CellPoint(row, column, true));
                } else if ((countOfNeighbors < 2 || countOfNeighbors > 3) && !grid.isEmpty(row, column)) {
                    affected.add(new CellPoint(row, column, false));
                }

            }
        }

        affected.forEach(cellPoint -> {
            if (cellPoint.isAlive) {
                grid.fillCell(cellPoint.row, cellPoint.column);
            } else {
                grid.killCell(cellPoint.row, cellPoint.column);
            }
        });

    }
}
