package edu.crabdev.training.cellular.core;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Canvas extends JPanel {

    private int cellSize;
    private Grid grid;


    private static final Color cellColorLive = new Color(0, 140, 0);
    private static final Color cellColorDeath = Color.WHITE;

    Canvas(Grid grid, int cellSize) {
        setBackground(Color.WHITE);
        this.grid = grid;
        this.cellSize = cellSize;
    }

    void init() {
        grid.clear();
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;

        int margin = 2;
        for (int column = 0; column < grid.getColumns(); column++) {
            for (int row = 0; row < grid.getRows(); row++) {
                Rectangle2D.Double rect = new Rectangle2D.Double(column * cellSize, row * cellSize, cellSize, cellSize);
                Rectangle b = rect.getBounds();
                g2.setPaint(Color.BLACK);
                g2.draw(rect);
                if (grid.isAliveCell(row, column)) {
                    g2.setPaint(cellColorLive);
                    g2.fillRect(b.x + margin, b.y + margin, b.width - 2 * margin, b.height - 2 * margin);
                }
            }
        }

        g2.draw(new Rectangle2D.Double(0, 0, grid.getColumns() * cellSize, grid.getRows() * cellSize));

    }

    @Override
    public Dimension getPreferredSize() {
        int width = grid.getColumns() * cellSize;
        int height = grid.getRows() * cellSize;
        return new Dimension(width, height);
    }
}
