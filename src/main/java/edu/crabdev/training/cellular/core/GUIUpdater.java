package edu.crabdev.training.cellular.core;

public interface GUIUpdater {

    void updatePopulationNumber(int value);
    void updateCountOfAlive(int value);

}
