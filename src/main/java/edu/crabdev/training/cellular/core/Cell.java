package edu.crabdev.training.cellular.core;

class Cell {

    private boolean live;

    Cell(boolean live) {
        this.live = live;
    }

    boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }
}
